package com.mycompany.app;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class App {
    private final static Logger logger = LogManager.getLogger();

    public static void main(String args[]) {
        logger.info("Hello, world");
        logger.warn("warning");
        logger.error("it's an error");
        logger.debug("dddddebug");

        for (int i=1; i<=10; i++) {
            logger.info(i + " ");
            int time = 500 + (int) (Math.random() * 500);
            try {
                Thread.sleep(time);
            }
            catch (InterruptedException e) {}
        }
    }
}